<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">

    <title>Pay Page</title>
</head>

<body>

    <?php include_once 'site_elements/nav.php' ?>

    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="display-4 my-3">Payments</h1>
                <hr>
            </div>
        </div>
    </div>

    <div class="container">
        <h3>Customers</h3>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created at</th>
                </tr>
            </thead>
            <tbody>
                <?php
                require_once 'config/db.php';
                require_once 'lib/pdo_db.php';
                require_once 'models/Customer.php';
                $customer = new Customer;
                $customers = $customer->getCustomers();

                foreach ($customers as $customer) {

                    echo
                    "<tr>
                    <td scope=\"row\">{$customer->id}</td>
                    <td>{$customer->first_name} {$customer->first_name}</td>
                    <td>{$customer->email}</td>
                    <td>{$customer->created_at}</td>
                </tr>";
                }

                ?>
            </tbody>
        </table>
    </div>

    <div class="container">
        <h3>Payments / Transactions?</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">customer_id</th>
                    <th scope="col">product</th>
                    <th scope="col">amount</th>
                    <th scope="col">currency</th>
                    <th scope="col">status</th>
                    <th scope="col">created_at</th>
                </tr>
            </thead>
            <tbody>
                <?php
                require_once 'config/db.php';
                require_once 'lib/pdo_db.php';
                require_once 'models/Transaction.php';
                $transaction = new Transaction;
                $transactions = $transaction->getTransactions();

                foreach ($transactions as $transaction) {

                    echo
                    "<tr>
                    <td scope=\"row\">{$transaction->id}</td>
                    <td>{$transaction->customer_id}</td>
                    <td>{$transaction->product}</td>
                    <td>{$transaction->amount}</td>
                    <td>{$transaction->currency}</td>
                    <td>{$transaction->status}</td>
                    <td>{$transaction->created_at}</td>
                </tr>";
                }

                ?>
            </tbody>
        </table>
    </div>

    <?php include_once 'site_elements/footer.php' ?>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="./js/charge.js"></script>
</body>

</html>