<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">

    <title>Pay Page</title>
</head>

<body>

    <?php include_once 'site_elements/nav.php' ?>

    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="display-4 my-3">Pay Page</h1>
                <hr>
            </div>
        </div>
    </div>

    <div class="container py-2">
        <form action="./charge.php" method="post" id="payment-form">
            <div class="customer-data mb-3">
                <div class="card">
                    <div class="card-header">
                        <p class="lead mb-0">Payment data</p>
                    </div>
                    <div class="card-body">

                        <div class="row mb-3">
                            <label for="description" class="col-sm-2 col-form-label">Description:</label>
                            <div class="col-sm-10">
                                <input type="text" name="description" id="description" class="form-control" value="Intro To Stripe Course">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="row">
                                    <label for="amount" class="col-sm-4 col-form-label">Amount:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="amount" id="amount" class="form-control mb-3" value="99" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <label for="currency" class="col-sm-4 col-form-label">Currency:</label>
                                    <div class="col-sm-8">
                                        <select class="form-select" name="currency" id="currency" aria-label="Default select example">
                                            <option value="usd" selected>usd</option>
                                            <option value="pln">PLN</option>
                                            <option value="gb">gb</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <p class="lead mb-0">Customer data</p>
                </div>
                <div class="card-body">

                    <div class="row g-3">
                        <div class="col">
                            <input type="text" name="first_name" class="form-control mb-3 StripeElement StripeElement--empty" placeholder="First Name" required>
                        </div>
                        <div class="col">
                            <input type="text" name="last_name" class="form-control mb-3 StripeElement StripeElement--empty" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col">
                            <input type="email" name="email" class="form-control mb-3 StripeElement StripeElement--empty" placeholder="Email Address">
                        </div>
                        <div class="col">
                            <div id="card-element" class="form-control">
                                <!-- a Stripe Element will be inserted here. -->
                            </div>
                            <div id="card" class="form-text">
                                <a href="https://stripe.com/docs/terminal/references/testing#standard-test-cards">Card numbers</a>.
                                <br><span>4242424242424242</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Used to display form errors -->
            <div id="card-errors" role="alert"></div>

            <button>Submit Payment</button>
        </form>
    </div>

    <?php include_once 'site_elements/footer.php' ?>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="./js/charge.js"></script>
</body>

</html>