<?php

ini_set("display_errors", 1);

if (isset($_GET['page'])) {
    if ($_GET['page'] == 'pay_page') {
        include_once 'views/pay_page.php';
    } elseif ($_GET['page'] == 'payments') {
        include_once 'views/payments.php';
    }
} else {
    include_once 'views/start.php';
}
